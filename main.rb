require './passenger.rb'
require './luggage.rb'
require './airplane.rb'

class Main
  attr_accessor :airplane, :passenger, :luggage

  # @example
  #   Main.new(airplane: {callsign: "Tu-33", distance: 122, vmax: 23, capacity: 999}, passenger: {name: "Jozek", lastname: "Kapiszon", gender: "m", callsign: "Tu-33", type: 2}, luggage: {weight: 7})
  def initialize(args = {})
    @airplane = Airplane.new(
      args[:airplane][:callsign],
      args[:airplane][:disance], 
      args[:airplane][:vmax],
      args[:airplane][:capacity])
   
    @passenger = Passenger.new(
      args[:passenger][:name],
      args[:passenger][:lastname],
      args[:passenger][:gender],
      args[:passenger][:callsign],
      args[:passenger][:type])
    
    @luggage = Luggage.new(@passenger, args[:luggage][:weight])
  end

  def seed
    @airplane.add_passenger(@passenger)
    @airplane.add_luggage(@luggage)
    @airplane.count_passengers_weight
    nil
  end
end
