class Passenger
  attr_accessor :name, :lastname, :gender, :type, :callsign
  attr_reader :weight

  def initialize(name, lastname, gender, callsign, type)
    @name = name
    @lastname = lastname
    @gender = gender
    @callsign = callsign
    @type = type
    @weight = rand_weight(gender)
  end
  
  def rand_weight(gender)
    if gender == 'm'
      basic_weight = 50 
    else 
      basic_weight = 60 
    end
    rand(50) + basic_weight
  end
end
