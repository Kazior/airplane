require './passenger.rb'
require './luggage.rb'

class Airplane
  attr_accessor :callsign, :distance, :vmax, :capacity
  attr_reader :serialnumber

  def initialize(callsign, distance, vmax, capacity)
    @callsign = callsign
    @distance = distance
    @vmax = vmax
    @capacity = capacity
    @serialnumber = random_serial
    @passenger_list = []
    @luggage_list = {}
  end

  def add_passenger(passenger)
    @passenger_list << passenger
    @empty_luggage = Luggage.new(passenger, 0)
    add_luggage(@empty_luggage)
  end

  def add_luggage(luggage)
    @luggage_list.store(luggage.owner, luggage)
  end

  def show_luggages
    @luggage_list.each { |passenger, luggage| puts "#{passenger} is #{luggage}" }
  end

  def show_passengers
    @passenger_list.each do |passenger|
      puts "#{passenger.name} #{passenger.lastname} in #{passenger.type} class"
    end
  end

  # counts number of passengers in each type (1, 2) or all (0)
  def count_passengers(type = 0)
    count = 0
    @passenger_list.each do |passenger|
      count += 1 if type == passenger.type || type == 0
    end
    count
  end

  # counts number of passengers in each type (1, 2) or all (0)
  def count_passengers_weight(type = 0)
    count = 0
    @passenger_list.each do |passenger|
			if type == passenger.type || type == 0
        count += passenger.weight + @luggage_list[passenger.lastname].weight
      end
    end
    count
  end

  def capacity_remaining
    @capacity - count_passengers_weight
  end

  private

  def random_serial
    rand(1000)+3000
  end
end
