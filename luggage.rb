require './passenger.rb'

class Luggage
  attr_accessor(:weight, :owner)

  def initialize(passenger, weight)
    @callsign = passenger.callsign
    @owner = passenger.lastname
    @weight = weight
  end

  def add_content(params = {})
    @main = params[:main] || "clothes"
    @weapon = params[:weapon] || ""
    @pocket1 = params[:pocket1] || "food"
    @pocket2 = params[:pocket2] || "documents"
    @pocket3 = params[:pocket3] || "cosmetics"
    @weapon_docs = @weapon.empty? ? "need to add some weapon..." : @weapon + " documents" 
  end

  def show_content
    puts @main
    puts @weapon
    puts @pocket1
    puts @pocket2
    puts @pocket3
    puts @weapon_docs
  end

  def show_luggage_description
    puts @callsign
    puts @owner
    puts @weight
  end
end
